package webview.android.antonsmirnov.name.webviewtestapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    private static final String CSS_INJECT =
            "var node = document.createElement('style'); " +
                    "node.innerHTML = '#abb-banner img{display:none;}'; " +
                    "document.getElementsByTagName('head')[0].appendChild(node);";

    private static final String CSS_INJECT2 = "document.getElementById('abb-banner').style.display = 'none';";

    private static final String SCRIPT_BEFORE =
            "if (document.readyState != 'complete' && jsToJavaBridge.getAddDomListener()){ " +
                    "jsToJavaBridge.setAddDomListener(false); " +
                    "document.addEventListener('DOMContentLoaded', function() { " +
                    // "document.onreadystatechange = function() { if (document.readyState === 'interactive') { " +
                    CSS_INJECT +
                    "}, false); " +
                    // "}; };" +
                    "};";

    private static final String SCRIPT_AFTER = CSS_INJECT;

    private WebView webView;
    private CheckBox cbApi19;
    private CheckBox cbScript1;
    private CheckBox cbScript2;

    private boolean addDomListener;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindControls();
        initControls();
    }

    private void loadWebView() {
        getSupportActionBar().setTitle("Loading #" + (++counter));

        webView.stopLoading();
        webView.loadUrl("https://adblockplus.org");
    }

    private void bindControls() {
        webView = (WebView) findViewById(R.id.wv);
        cbApi19 = (CheckBox) findViewById(R.id.api19);
        cbScript1 = (CheckBox) findViewById(R.id.script1);
        cbScript2 = (CheckBox) findViewById(R.id.script2);
    }

    private void initControls() {
        initWebView();
    }

    private void initWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        // we can inject ABPEngine/FilterEngine to be available from JS like this
        webView.addJavascriptInterface(MainActivity.this, "jsToJavaBridge");

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                if (cbScript1.isChecked()) {
                    runScript(SCRIPT_BEFORE);
                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                setAddDomListener(true);
                getSupportActionBar().setTitle("Started #" + counter);
                getSupportActionBar().setSubtitle(url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                setAddDomListener(false);
                getSupportActionBar().setTitle("Finishing ");
                getSupportActionBar().setSubtitle(url);

                if (cbScript2.isChecked()) {
                    runScript(SCRIPT_AFTER);
                }

                getSupportActionBar().setTitle("Finished");
            }
        });
    }

    private void runScript(String script) {
        if (cbApi19.isChecked())
            webView.evaluateJavascript(script, null);
        else
            webView.loadUrl("javascript:" + script);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload) {
            loadWebView();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @JavascriptInterface
    public void setAddDomListener(boolean addDOMListener) {
        this.addDomListener = addDOMListener;
    }

    @JavascriptInterface
    public boolean getAddDomListener() {
        return this.addDomListener;
    }
}
